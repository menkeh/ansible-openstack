variable "key_pair" {
  description = "Name of the keypair to use for compute instances"
  type        = string
}
