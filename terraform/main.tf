data "openstack_images_image_v2" "ubuntu-2204" {
  name = "Ubuntu 22.04"
}

data "openstack_compute_flavor_v2" "mpcdf-small" {
  name = "mpcdf.small"
}

data "openstack_networking_network_v2" "cloud-local-1" {
  name = "cloud-local-1"
}

data "openstack_networking_network_v2" "cloud-local-2" {
  name = "cloud-local-2"
}

data "openstack_networking_network_v2" "cloud-local-3" {
  name = "cloud-local-3"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

data "openstack_compute_keypair_v2" "kp" {
  name = var.key_pair
}

locals {
  user_data = templatefile("${path.module}/user_data.tftpl", {
    public_key = data.openstack_compute_keypair_v2.kp.public_key
  })
}

resource "openstack_compute_instance_v2" "cloud-runner" {
  count           = 2
  name            = format("cloud-runner-%02d", count.index + 1)
  image_id        = data.openstack_images_image_v2.ubuntu-2204.id
  flavor_id       = data.openstack_compute_flavor_v2.mpcdf-small.id
  key_pair        = data.openstack_compute_keypair_v2.kp.name
  security_groups = [data.openstack_networking_secgroup_v2.default.name]
  user_data       = local.user_data

  network {
    name = data.openstack_networking_network_v2.cloud-local-3.name
  }
}

resource "ansible_host" "vm" {
  count = 2
  name  = openstack_compute_instance_v2.cloud-runner[count.index].name
  variables = {
    ansible_host = openstack_compute_instance_v2.cloud-runner[count.index].network[0].fixed_ip_v4
    ansible_user = "ansible"
    gpu          = (count.index == 1 ? "a30:1" : null) # mock GPU
  }
}
