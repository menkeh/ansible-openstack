# Prerequisites

The Terraform file is not 100% self-contained. There are several prerequisites
that have to be met.

1. You need an application credential to talk to the OpenStack API.
   To create a new application credential go here:
   https://hpccloud.mpcdf.mpg.de/dashboard/identity/application_credentials/

   The *ID* can be exported as `OS_APPLICATION_CREDENTIAL_ID` and the *Secret*
   can be exported as `OS_APPLICATION_CREDENTIAL_SECRET`.

2. A key pair must already exist on OpenStack.  No new key pairs are created
   during deployment.  To create a new or upload an existing key, go to
   https://hpccloud.mpcdf.mpg.de/dashboard/project/key_pairs

   The *name of the key pair* can be exported as `TF_VAR_key_pair`.

# Setup

Start the VM cluster.

```console
$ cd terraform
$ terraform apply
```

Apply the configuration.

```console
$ cd ansible
$ ansible-playbook main.yml --ask-vault-pass
```
